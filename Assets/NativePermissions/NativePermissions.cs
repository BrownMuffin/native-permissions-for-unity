﻿using UnityEngine;
using System;

public class NativePermissions
{
    // Singleton
    private static NativePermissions _instance;
    public static NativePermissions Instance
    {
        get
        {
            if (_instance == null)
                _instance = new NativePermissions();

            return _instance;
        }
    }

    private Action<bool> _permissionCallBack;
    private AndroidPermissions _permission;
    private bool _permissionChecking = false;

    private NativePermissions() { }

    /// <summary>
    /// Check if the Android permission is granted.
    /// </summary>
    /// <param name="permission">Permission type.</param>
    /// <returns>'true' if the permission is granted, otherwise 'false'.</returns>
    public bool IsPermissionGranted(AndroidPermissions permission)
    {
#if UNITY_EDITOR
        Debug.LogWarning("[NativePermissions] This doesn't work in the editor.");
        return true;
#else
        // Get API Level, if lower than 23 return true
        AndroidJavaClass version = new AndroidJavaClass("android.os.Build$VERSION");
        
        if (version.GetStatic<int>("SDK_INT") < 23)
            return true;

        AndroidJavaClass unityPlayerClass   = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity     = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject unityContext      = unityActivity.Call<AndroidJavaObject>("getApplicationContext");

        int result = unityContext.Call<int>("checkCallingOrSelfPermission", "android.permission." + permission.ToString());

        return result == 0;
#endif
    }

    /// <summary>
    /// Request Android permissions.
    /// </summary>
    /// <param name="permission">Permission type.</param>
    /// <param name="onPermission">Callback, will be called with the result.</param>
    public void RequestPermission(AndroidPermissions permission, Action<bool> onPermission = null)
    {
#if UNITY_EDITOR
        Debug.LogWarning("[NativePermissions] This doesn't work in the editor.");
        return;
#else

        // Get API Level, if lower than 23 return true
        AndroidJavaClass version = new AndroidJavaClass("android.os.Build$VERSION");

        if (version.GetStatic<int>("SDK_INT") < 23)
        {
            onPermission.Invoke(true);
            return;
        }

        _permissionCallBack = onPermission;

        AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
        unityActivity.Call("requestPermissions", new string[] { "android.permission." + permission.ToString() }, 0);

        _permissionChecking = true;
        _permission = permission;
#endif 
    }

    /// <summary>
    /// If the focus comes back after the permissions window, check if the permission is granted. Invoke the call back with the result.
    /// </summary>
    /// <param name="focus">'true' if the Unity view gets focus, otherwise it is 'false'.</param>
    private void OnApplicationFocus(bool focus)
    {
        if (focus && _permissionChecking)
        {
            _permissionChecking = false;

            bool granted = IsPermissionGranted(_permission);

            if (_permissionCallBack != null)
                _permissionCallBack.Invoke(granted);
        }
    }
}

public enum AndroidPermissions
{
    ACCESS_COARSE_LOCATION,
    ACCESS_FINE_LOCATION,
    ADD_VOICEMAIL,
    BODY_SENSORS,
    CALL_PHONE,
    CAMERA,
    GET_ACCOUNTS,
    PROCESS_OUTGOING_CALLS,
    READ_CALENDAR,
    READ_CALL_LOG,
    READ_CONTACTS,
    READ_EXTERNAL_STORAGE,
    READ_PHONE_STATE,
    READ_SMS,
    RECEIVE_MMS,
    RECEIVE_SMS,
    RECEIVE_WAP_PUSH,
    RECORD_AUDIO,
    SEND_SMS,
    USE_SIP,
    WRITE_CALENDAR,
    WRITE_CALL_LOG,
    WRITE_CONTACTS,
    WRITE_EXTERNAL_STORAGE
}