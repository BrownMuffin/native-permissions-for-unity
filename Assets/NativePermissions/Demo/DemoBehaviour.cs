﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DemoBehaviour : MonoBehaviour
{
    [SerializeField] private Text _log;

    public void CheckCameraAccess()
    {
        bool permission = NativePermissions.Instance.IsPermissionGranted(AndroidPermissions.CAMERA);

        //_log.text = "CAMERA : " + permission.ToString();

        if (!permission)
            NativePermissions.Instance.RequestPermission(AndroidPermissions.CAMERA, OnCameraPermission);
    }

    private void OnCameraPermission(bool granted)
    {
        _log.text = "CAMERA : " + granted.ToString();
    }

    public void Camera()
    {
        WebCamTexture wt = new WebCamTexture();
        wt.Play();
    }
}
