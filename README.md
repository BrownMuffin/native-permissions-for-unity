# Native Android Permissions for Unity 3D#

Since Android version 6.0 (API level 23) we have runtime permissions. This Unity plugin checks the permission and shows the user the native permission window. This plugin doesn't use any Android plugin, everything is done through the JNI interface.


### Installation ###

* Download the Unitypackage
* Add permissions in your Android Manifest
* Add the following line in your Android Manifest
```
#!XML

<meta-data android:name="unityplayer.SkipPermissionsDialog" android:value="true" />
```

* Call the "IsPermissionGranted" function to check if the permission is granted or not
* Call the "RequestPermission" function to request the permission


### License ###

Copyright © 2017 Marvin Fredriksz < marvin [at] aboutmarvin [.] nl >
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.